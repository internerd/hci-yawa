## Readme - A3

* Name:	Lucas Schuster
* Matr.-Nr.:	a01507395
* Gruppe:	01


### Implementierung

**Framework:**	Android Kotlin

**API-Version:** Android Lollipop 5.1

**Gerät(e), auf dem(denen) getestet wurde:**  
Nexus 5X, One Plus One

**Externe Libraries und Frameworks:**  
Kotlin, Retrofit, RxAndroid, RxJava, Iconics, Moshi, Joda Time, Android Graphview

**Dauer der Entwicklung:**
25 Stunden

**Weitere Anmerkungen:**  
Hin und wieder scheint AndroidGraphview Probleme mit der Formatierung der Werte
auf der X-Achse zu haben, dabei hängt sich der View auf. Allerdings konnte ich
dieses Verhalten nicht an irgendwelchen Parametern festmachen.  
Da wir leider nur dreistündig Daten erhalten, kommt es gegen Abend zu dem Fall,
dass der Forecast nur 2 oder 1 Datenpunkt hält.
