package a3.hci.luci.uni.eini.at.yawa.api.owa

/**
 * Static Values used while working with or rather
 * configuration of the OpenWeather API
 */
object OpenWeatherConstants {
    val UNIT_METRIC = "metric"
    val UNIT_IMPERIAL = "imperial"
    val LANG_DE = "de"
    val LANG_EN = "en"
}