package a3.hci.luci.uni.eini.at.yawa.data

import java.util.*

data class DayForecast (
        val day: Date,
        val cityName: String, //maybe not needed
        val data: List<ForecastItem>
)
