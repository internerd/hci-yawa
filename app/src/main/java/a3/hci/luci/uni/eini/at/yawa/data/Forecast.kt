package a3.hci.luci.uni.eini.at.yawa.data

import a3.hci.luci.uni.eini.at.yawa.api.owa.City

data class Forecast(
        val forecasts: HashMap<String, MutableList<ForecastItem>>,
        val city: City // maybe unneeded
)