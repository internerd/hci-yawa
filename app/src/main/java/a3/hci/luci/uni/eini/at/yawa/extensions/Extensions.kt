package a3.hci.luci.uni.eini.at.yawa.extensions

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

/**
 * Overrides the inflation method for the ViewGroup,
 */
fun ViewGroup.inflate(layoutId: Int, attachToRoot: Boolean = false): View {
    return LayoutInflater.from(context).inflate(layoutId, this, attachToRoot)
}