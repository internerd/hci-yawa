package a3.hci.luci.uni.eini.at.yawa.api.owa

import okhttp3.OkHttpClient
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

/**
 * Class to implement multiple APIs
 * (atm only OWA is implemented)
 */
class RestAPI(
        private val owaApiKey: String,
        private val unitFormat: String,
        private val lang: String
) {

    private val owaApi: OpenWeatherAPI

    init {


        val okHttpClient = OkHttpClient.Builder()
                .addInterceptor(OpenWeatherInterceptor(owaApiKey, unitFormat, lang))
                .build()
        val openweather = Retrofit.Builder()
                .baseUrl("https://api.openweathermap.org/data/2.5/")
                .client(okHttpClient)
                .addConverterFactory(MoshiConverterFactory.create())
                .build()

        owaApi = openweather.create(OpenWeatherAPI::class.java)
    }

    /**
     * calls API with the retrofit interface for OWA
     * actual code is generated at build time
     */
    fun getWeatherByName(query: String): Call<OpenWeatherWeatherResponse> {
        return owaApi.getWeatherByName(query)
    }

    /**
     * calls API with the retrofit interface for OWA
     * actual code is generated at build time
     */
    fun getForecastByName(query: String): Call<OpenWeatherForecastResponse> {
        return owaApi.getForecastByName(query)
    }
}