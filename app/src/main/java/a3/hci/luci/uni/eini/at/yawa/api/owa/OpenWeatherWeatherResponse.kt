package a3.hci.luci.uni.eini.at.yawa.api.owa

import a3.hci.luci.uni.eini.at.yawa.data.ForecastItem
import com.squareup.moshi.Json

/**
 * this class only acts as a holder for the data returned
 * by the Open Weather API
 */
data class OpenWeatherWeatherResponse (
        val dt: Long,
        val coord: Coordinates,
        val weather: List<Weather>,
        val main: DetailWeather,
        val wind: Wind,
        val name: String,
        val sys: DetailSystem
)

/**
 * this class only acts as a holder for the data returned
 * by the Open Weather API
 */
data class OpenWeatherForecastResponse (
        val list: List<ForecastItem>,
        val city: City
)

class Coordinates (
    val lat: Double,
    val lon: Double
)

data class Weather (
    val id: Int,
    val main: String,
    val description: String
)

data class Wind (
    val speed: Double,
    val deg: Double
)

data class Rain(
        @Json(name = "3h") val threeHour: Double
)

data class DetailWeather (
    val temp: Double,
    val pressure: Double,
    val humidity: Double,
    val temp_min: Double,
    val temp_max: Double
)

data class DetailSystem (
    val country: String,
    val sunrise: Long,
    val sunset: Long
)

data class City(
        val name: String,
        val coord: Coordinates,
        val country: String,
        val population: Long
)