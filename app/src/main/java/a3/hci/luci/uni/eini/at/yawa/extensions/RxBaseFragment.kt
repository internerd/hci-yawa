package a3.hci.luci.uni.eini.at.yawa.extensions

import android.support.v4.app.Fragment
import rx.subscriptions.CompositeSubscription

/**
 * Can be later used for implementation of Parcelables and
 * data-loss safe rotation of device
 */
open class RxBaseFragment() : Fragment() {
    var subscriptions = CompositeSubscription()

    override fun onResume() {
        super.onResume()
        subscriptions = CompositeSubscription()
    }

    override fun onPause() {
        super.onPause()
        subscriptions.clear()
    }
}