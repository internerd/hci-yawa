package a3.hci.luci.uni.eini.at.yawa.data

import a3.hci.luci.uni.eini.at.yawa.api.owa.DetailWeather
import a3.hci.luci.uni.eini.at.yawa.api.owa.Rain
import a3.hci.luci.uni.eini.at.yawa.api.owa.Weather

data class ForecastItem(
        val dt: Long,
        val id: Int,
        val main: DetailWeather,
        val weather: MutableList<Weather>,
        val rain: Rain
)