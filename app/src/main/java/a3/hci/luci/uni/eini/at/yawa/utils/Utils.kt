package a3.hci.luci.uni.eini.at.yawa.utils

import org.joda.time.DateTime

class Utils {
    companion object {
        const val DAY_MORNING = 0
        const val DAY_MIDDAY = 1
        const val DAY_AFTERNOON = 2
        const val DAY_EVENING = 3
        const val DAY_NIGHT = 4

        /**
         * resolve conditions-icon name, based on
         * returned weather id
         */
        fun resolveCondition(condition: Int): String {
            var iconType = "{wic-"
            iconType += when (condition) {
                in 200..299 -> "day-thunderstorm" //thunderstorm
                in 300..399 -> "sprinkle" //drizzle
                in 500..599 -> {
                    when (condition) {
                        501 -> "sleet" // moderate rain
                        502 -> "rain" // heavy rain
                        503 -> "showers" // very heavy rain
                        504 -> "rain-wind" // extreme rain
                        else -> "sprinkle" // light rain
                    }
                }
                in 600..699 -> "snow" //snow
                in 700..799 -> "dust" //atmosphere -> more to follow
                in 800..899 -> {
                    when (condition) {
                        801 -> "cloud" // few clouds
                        802 -> "fog" // scattered clouds
                        803 -> "cloudy-windy" // broken clouds
                        804 -> "cloudy" // overcast clouds
                        else -> "day-sunny" //clear sky
                    }
                }
                in 900 until condition -> "volcano" //extreme
                else -> "alien"
            }
            iconType += "}"

            return iconType
        }

        /**
         * resolves the time segment
         * of the day, to be able to
         * make time-relevant decisions
         * in the app
         */
        fun resolveCurrentTime(): Int{
            val hour = DateTime().hourOfDay().get()

            return when (hour) {
                in 6..10 -> DAY_MORNING
                in 11..13 -> DAY_MIDDAY
                in 14..17 -> DAY_AFTERNOON
                in 18..21 -> DAY_EVENING
                else -> DAY_NIGHT
            }
        }
    }
}