package a3.hci.luci.uni.eini.at.yawa.ui.detail

import a3.hci.luci.uni.eini.at.yawa.R
import a3.hci.luci.uni.eini.at.yawa.data.DayForecast
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.jjoe64.graphview.DefaultLabelFormatter
import com.jjoe64.graphview.GridLabelRenderer
import com.jjoe64.graphview.series.DataPoint
import com.jjoe64.graphview.series.LineGraphSeries
import kotlinx.android.synthetic.main.weather_card_adapter.view.*
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

/**
 * This Adapter View class is used for the display of the forecast items
 * returned from the API.
 * It also populates the graphviews, as this is the only place this is currently used.
 */
class ForecastViewAdapter: RecyclerView.Adapter<ForecastViewAdapter.ViewHolder>() {

    private var items: ArrayList<DayForecast> = ArrayList()

    companion object {
        val dateFormat: DateFormat = SimpleDateFormat.getDateInstance(DateFormat.DEFAULT)
        val hourFormat: DateFormat = SimpleDateFormat("HH:mm", Locale.GERMAN)
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent?.context)
        val view = layoutInflater.inflate(R.layout.weather_card_adapter, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int  = items.size // eg. 5 days of forecasts

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(items[position])

    fun updateData(newItems: ArrayList<DayForecast>): Unit {
        items.clear()
        items = newItems
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        /**
         * binds a DayForecast object to the item view in the recycler
         * takes care of setting up the graphs
         *
         * NOTE:
         * The scale currently has a hardcoded upper bound (30[°])
         * because i wasnt able to accomplish a dynamically generated upper bound
         */
        fun bind(item: DayForecast) = with(itemView) {
            // clear the graphview card
            forecast_day_item_graph.removeAllSeries()

            val lowTemps : List<DataPoint> = item.data.map { DataPoint(Date(it.dt * 1000), it.main.temp_min) }
            val midTemps : List<DataPoint> = item.data.map { DataPoint(Date(it.dt * 1000), it.main.temp) }
            val maxTemps : List<DataPoint> = item.data.map { DataPoint(Date(it.dt * 1000), it.main.temp_max) }

            val lowSeries = LineGraphSeries(lowTemps.toTypedArray())
            lowSeries.color = context.resources.getColor(R.color.colorPrimaryDark)
            lowSeries.isDrawDataPoints = true
            lowSeries.dataPointsRadius = 10F

            val midSeries = LineGraphSeries(midTemps.toTypedArray())
            midSeries.color = context.resources.getColor(R.color.colorPrimaryLight)
            midSeries.isDrawDataPoints = true
            midSeries.dataPointsRadius = 10F

            val maxSeries = LineGraphSeries(maxTemps.toTypedArray())
            maxSeries.color = context.resources.getColor(R.color.colorAccent)
            maxSeries.isDrawDataPoints = true
            maxSeries.dataPointsRadius = 10F


            forecast_day_item_graph.gridLabelRenderer.labelFormatter= object : DefaultLabelFormatter() {
                override fun formatLabel(value: Double, isValueX: Boolean): String {
                    return if (isValueX) {
                        // show normal x values
                        hourFormat.format(value)
                    } else {
                        // show currency for y values
                        Math.round(value).toString()+"° "
                    }
                }
            }

            val initLabelCount = Math.ceil(lowTemps.size / 2.0).toInt() + 1
            forecast_day_item_graph.gridLabelRenderer.numHorizontalLabels =
                    if (initLabelCount > 3) initLabelCount else 3

            if (lowTemps[0].y < 0) {
                forecast_day_item_graph.viewport.setMinY(lowTemps[0].y - 5.0)
            } else {
                forecast_day_item_graph.viewport.setMinY(0.0)
            }
            forecast_day_item_graph.viewport.setMaxY(30.0)

            forecast_day_item_graph.viewport.isYAxisBoundsManual = true

            forecast_day_item_graph.viewport.setMinX(lowTemps[0].x)
            forecast_day_item_graph.viewport.setMaxX(lowTemps[lowTemps.size-1].x)
            forecast_day_item_graph.viewport.isXAxisBoundsManual = true


            forecast_day_item_graph.viewport.isScalable = false
            forecast_day_item_graph.gridLabelRenderer.isHumanRounding = false
            forecast_day_item_graph.gridLabelRenderer.gridStyle = GridLabelRenderer.GridStyle.HORIZONTAL


            forecast_day_item_graph.addSeries(lowSeries)
            forecast_day_item_graph.addSeries(midSeries)
            forecast_day_item_graph.addSeries(maxSeries)

            forecast_day_item_date.text = item.cityName+", "+dateFormat.format(item.day)
        }
    }
}