package a3.hci.luci.uni.eini.at.yawa.ui.detail

import a3.hci.luci.uni.eini.at.yawa.R
import a3.hci.luci.uni.eini.at.yawa.api.owa.OpenWeatherConstants
import a3.hci.luci.uni.eini.at.yawa.api.owa.RestAPI
import a3.hci.luci.uni.eini.at.yawa.data.DayForecast
import a3.hci.luci.uni.eini.at.yawa.data.Forecast
import a3.hci.luci.uni.eini.at.yawa.data.WeatherDetail
import a3.hci.luci.uni.eini.at.yawa.extensions.RxBaseFragment
import a3.hci.luci.uni.eini.at.yawa.extensions.inflate
import a3.hci.luci.uni.eini.at.yawa.utils.Utils
import a3.hci.luci.uni.eini.at.yawa.weather.detail.WeatherManager
import android.app.AlertDialog
import android.app.SearchManager
import android.content.Context
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.PagerSnapHelper
import android.support.v7.widget.SearchView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.weather_detail_fragment.*
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import java.text.DateFormat
import java.text.SimpleDateFormat


/**
 * This class takes care about searching for Locations and displaying
 * weather data in the connected view
 */
class WeatherDetailFragment: RxBaseFragment() {

    var currentWeatherDetail : WeatherDetail? = null
    var currentForecast : Forecast? = null

    val dateFormat: DateFormat = SimpleDateFormat.getDateInstance(SimpleDateFormat.DEFAULT)

    private var isSearch: Boolean = false

    private lateinit var manager: WeatherManager

    private var forecastItemAdapter: ForecastViewAdapter = ForecastViewAdapter()

    /**
     * Static Values for refreshing data,
     * and setting default Location (since no SettingsView given)
     */
    companion object {
        val CURLOCATION_CITY = "CURLOCATION_CITY"
        const val DEFAULT_LOCATION = "Wien"
        const val REFRESH_DETAIL = 0
        const val REFRESH_FORECAST = 1
        const val REFRESH_BOTH = 2
    }

    /**
     * inflates layout for fragment
     */
    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return container?.inflate(R.layout.weather_detail_fragment)
    }

    /**
     * inits views and applies ClickListener on info button
     */
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        manager = WeatherManager(RestAPI(getString(R.string.owm_key),
                OpenWeatherConstants.UNIT_METRIC, OpenWeatherConstants.LANG_DE))

        initView()

    }

    /**
     * when the fragment is resumed, the data gets refreshed
     */
    override fun onResume() {
        super.onResume()
        println("Resuming Fragment")

        if(!isSearch && currentWeatherDetail != null)
            refreshData(REFRESH_BOTH, currentWeatherDetail!!.city)
        else
            isSearch = false

    }

    /**
     * this method takes care of setting up the initial view binding
     * and retrieving of the initial data
     */
    private fun initView() {
        val searchManager = activity.getSystemService(Context.SEARCH_SERVICE) as SearchManager
        search_location.setIconifiedByDefault(false)
        search_location.isIconified = false

        search_location.setOnQueryTextListener(object: SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                search_location.clearFocus()
                if(query != null) {
                    isSearch = true
                    search_location.setQuery("Loading", false)
                    refreshData(REFRESH_BOTH, query)
                }
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                return false
            }

        })

        search_location.setSearchableInfo(searchManager.getSearchableInfo(activity.componentName))

        forecast_day_items.layoutManager = LinearLayoutManager(context, LinearLayout.HORIZONTAL, false)
        forecast_day_items.adapter = forecastItemAdapter
        val snapHelper = PagerSnapHelper()
        snapHelper.attachToRecyclerView(forecast_day_items)

        refreshData(REFRESH_BOTH, DEFAULT_LOCATION)
        weather_detail_root.requestFocus()

        weather_detail_info.setOnClickListener {
            val alertDialog = AlertDialog.Builder(context).create()
            alertDialog.setView(layoutInflater.inflate(R.layout.dialog_information, this.view as ViewGroup, false))
            alertDialog.show()
        }
    }

    /**
     * updates given weather data based on a constant Int
     *
     * either only the detail information or the forecast
     * can be queried. also updating both data sources is
     * supported
     */
    private fun refreshData(type: Int, query: String) {
        when (true) {
            type == REFRESH_DETAIL -> { //only refresh weather detail
                requestWeatherDetail(query)
            }
            type == REFRESH_FORECAST -> { //only refresh forecast
                requestForecast(query)
            }
            type == REFRESH_BOTH -> { //refresh both
                requestWeatherDetail(query)
                requestForecast(query)
            }
        }
        updateUiBackground()
    }

    /**
     * calls the weather detail service
     * and updates given currentWeatherDetail
     */
    private fun requestWeatherDetail(query : String) {
        manager.getWeatherDetail(query)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe (
                        { weatherDetail ->
                            println(weatherDetail.toString())
                            updateUiDetail(weatherDetail)
                            currentWeatherDetail = weatherDetail
                            // update search location input after fetching new weather info
                            search_location.setQuery(
                                    "${currentWeatherDetail!!.city}, ${currentWeatherDetail!!.country}",
                                    false)
                        },
                        { e ->
                            e.printStackTrace()
                            Snackbar.make(this.view!!, e.message ?: "Error Fetching Data",
                                    Snackbar.LENGTH_LONG).show()
                            search_location.setQuery(
                                    "${currentWeatherDetail!!.city}, ${currentWeatherDetail!!.country}",
                                    false)
                        }
                )
    }

    /**
     * calls the weather forecast service
     * and updates given currentForecast
     */
    private fun requestForecast(query: String) {
        manager.getForecast(query)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        { forecast ->
                            println(forecast.toString())
                            println(forecast.forecasts.size)
                            updateUiForecast(forecast)
                            currentForecast = forecast
                        },
                        { e ->
                            e.printStackTrace()
                            Snackbar.make(this.view!!, e.message ?: "Error fetching Forecast",
                                    Snackbar.LENGTH_LONG).show()
                        }
                )
    }

    /**
     * updates the UiDetail View
     * with new data
     */
    private fun updateUiDetail(weatherDetail: WeatherDetail) {
        detail_location_temp.text = getString(R.string.detail_weather_temp,
                Math.round(weatherDetail.temp))
        detail_location_condition.text = getString(R.string.detail_weather_condition,
                Utils.resolveCondition(weatherDetail.code))
        search_location.setQuery(weatherDetail.city, false)
        println(Utils.resolveCondition(weatherDetail.code))
    }

    /**
     * updates the UiForecast View
     * with new data
     */
    private fun updateUiForecast(forecast: Forecast) {
        val cityName: String = forecast.city.name
        forecastItemAdapter.updateData(forecast.forecasts.asIterable().map {
            DayForecast(dateFormat.parse(it.key), cityName, it.value)
        }.sortedByDescending { dayForecast -> dayForecast.day }
                .filter { it.data.size > 1 }.reversed() as ArrayList<DayForecast>)

        forecast_day_items.adapter.notifyDataSetChanged()
    }

    /**
     * updates the UiBackground View
     * based on given Time
     */
    private fun updateUiBackground(){
        val timeBackground = when (Utils.resolveCurrentTime()) {
            Utils.DAY_MORNING -> R.drawable.gradient_01_morning
            Utils.DAY_MIDDAY -> R.drawable.gradient_02_midday
            Utils.DAY_AFTERNOON -> R.drawable.gradient_03_afternoon
            Utils.DAY_EVENING -> R.drawable.gradient_04_evening
            else -> R.drawable.gradient_05_night
        }
        activity.root_layout.statusBarBackground = context.getDrawable(android.R.color.transparent)
        activity.root_layout.setBackgroundResource(timeBackground)
        layout_top_search.setBackgroundResource(timeBackground)

    }
}