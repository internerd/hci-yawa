package a3.hci.luci.uni.eini.at.yawa.api.owa

import okhttp3.Interceptor
import okhttp3.Response

/**
 * This interceptor should be added when working with the OWA API from
 * okhttp/Retrofit.
 * It adds the relevant query options to each call when working with OWA,
 * therefore one does not have to worry about this, when calling the API
 */
class OpenWeatherInterceptor(
        private val apiKey: String,
        private val unitFormat: String,
        private val lang: String
) : Interceptor {
    override fun intercept(chain: Interceptor.Chain?): Response {
        val original = chain!!.request()
        val originalHttpUrl = original.url()

        val url = originalHttpUrl.newBuilder()
                .addQueryParameter("appid", apiKey)
                .addQueryParameter("units", unitFormat)
                .addQueryParameter("lang", lang)
                .build()

        // Request customization: add request headers
        val requestBuilder = original.newBuilder()
                .url(url)

        val request = requestBuilder.build()
        return chain.proceed(request)
    }
}