package a3.hci.luci.uni.eini.at.yawa.data

data class WeatherDetail(
        val city: String,
        val country: String,
        val mainCondition: String,
        val temp: Double,
        val code: Int
)