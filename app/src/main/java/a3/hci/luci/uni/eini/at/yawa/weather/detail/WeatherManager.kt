package a3.hci.luci.uni.eini.at.yawa.weather.detail

import a3.hci.luci.uni.eini.at.yawa.api.owa.OpenWeatherConstants
import a3.hci.luci.uni.eini.at.yawa.api.owa.OpenWeatherForecastResponse
import a3.hci.luci.uni.eini.at.yawa.api.owa.RestAPI
import a3.hci.luci.uni.eini.at.yawa.data.Forecast
import a3.hci.luci.uni.eini.at.yawa.data.ForecastItem
import a3.hci.luci.uni.eini.at.yawa.data.WeatherDetail
import rx.Observable
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by lucas on 27.03.18.
 */
class WeatherManager(
        private val api: RestAPI = RestAPI("", OpenWeatherConstants.UNIT_METRIC,
                OpenWeatherConstants.LANG_EN)
) {

    val dateFormat: DateFormat = SimpleDateFormat.getDateInstance(SimpleDateFormat.DEFAULT)


    fun getForecast(query: String): Observable<Forecast> {
        return Observable.create {
            subscriber ->

            // returns OpenWeatherForecastResponse, which will be converted afterwards
            val response = api.getForecastByName(query).execute()

            if(response.isSuccessful) {
                val res = response.body()!!

                // convert OpenWeatherForecastResponse to sorted Hashmap
                val forecast = mapToForecast(res)

                subscriber.onNext(forecast)
                subscriber.onCompleted()
            } else {
                subscriber.onError(Throwable(response.message()))
            }
        }
    }

    fun getWeatherDetail(query: String) : Observable<WeatherDetail> {
        return Observable.create {
            subscriber ->

            val response = api.getWeatherByName(query).execute()

            if(response.isSuccessful) {
                val res = response.body()!!
                val weather = WeatherDetail(
                        res.name,
                        res.sys.country,
                        res.weather[0].main,
                        res.main.temp,
                        res.weather[0].id
                )
                subscriber.onNext(weather)
                subscriber.onCompleted()
            } else {
                subscriber.onError(Throwable(response.message()))
            }

        }
    }

    private fun mapToForecast(res: OpenWeatherForecastResponse): Forecast {
        val forecastByDay = HashMap<String, MutableList<ForecastItem>>()

        res.list.forEach({
            val timestamp = Date(it.dt * 1000)
            val formatted: String = dateFormat.format(timestamp)

            if(forecastByDay[formatted] != null) {
                forecastByDay[formatted]!!.add(it)
            } else {
                forecastByDay[formatted] = mutableListOf(it)
            }
        })

        return Forecast(
                forecastByDay,
                res.city
        )
    }
}